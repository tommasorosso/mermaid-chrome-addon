## Instructions to install it


### From source

1. Enable developer mode in chrome://extensions
2. Clone the repo
   ```bash
   git clone https://gitlab.com/tommasorosso/mermaid-chrome-addon
   ```
3. Click on load unpacked extension
4. Select the extensions folder inside the recently cloned repo

Done!

Special thank to this repo https://github.com/Redisrupt/mermaid-diagrams for the amazing job done porting mermaid on 
github!

