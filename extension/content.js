(function () {

  const $ = (selector, ctx = document) => [].slice.call(ctx.querySelectorAll(selector));

  function existingDiagramNode (elem) {
    return $('.mermaid', elem.parentElement)[0]
  }

  function setupChart(elem, code) {
    let existingDiagram = existingDiagramNode(elem)
    if (existingDiagram) {
      existingDiagram.innerHTML = code
    } else {
      // Create the element that will house the rendered diagram.
      elem.insertAdjacentHTML('afterend', `<div class="mermaid">${code}</div>`);
      elem.style.display = 'none';
      existingDiagram = existingDiagramNode(elem)

      // Create an observer to track changes to the diagram code.
      const observer = new MutationObserver(() => { processElement(elem) });
      observer.observe(elem, { characterData: true });
    }

    // Generate or regenerate diagram if it is existing.
    window.mermaid.init([existingDiagram]);
  };

  function processElement(elem) {
    if (elem.attributes["lang"] !== null) {
      const codeElem = $('code', elem)[0];
      const code = codeElem.textContent;
      setupChart(elem, code);
    } else {
      const code = elem.textContent;
      setupChart(elem, code);
    }
  };


  let processing = false;
  document.addEventListener('DOMNodeInserted', (e) => {
    if(e.relatedNode.className === 'card-detail-window u-clearfix' && !processing) {
      processing = true;
      setTimeout(function(){ 
        setTimeout(() => processing = false, 1000);
        var nodes = document.getElementsByTagName("pre");
        for (var n=0; n<nodes.length; n++) {
          let node = nodes[n];
          processElement(node);
        }
      }, 100);
    }
  });

}());
